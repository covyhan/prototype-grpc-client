package com.example.prototypeclient

import com.example.talk.proto.TalkGrpcKt
import com.example.talk.proto.sendTalkRequest
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import io.grpc.StatusException
import io.grpc.StatusRuntimeException
import java.io.Closeable
import java.util.concurrent.TimeUnit

class Application(private val channel: ManagedChannel) : Closeable {
    private val stub: TalkGrpcKt.TalkCoroutineStub = TalkGrpcKt.TalkCoroutineStub(channel)

    suspend fun send(content: String) {
        val request = sendTalkRequest {
            this.content = content
        }
        try {
            val response = stub.sendTalk(request)
            println("Received: $response")
        } catch (e: StatusException) {
            println("Received error response from gRPC server. code - ${e.status.code}, description - ${e.status.description}, cause - ${e.status.cause}")
        }
    }

    override fun close() {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS)
    }
}

suspend fun main(args: Array<String>) {
    val port = 9090

    val channel = ManagedChannelBuilder.forAddress("localhost", port).usePlaintext().build()

    val client = Application(channel)

    client.send(content = "I'm covy!!")
}
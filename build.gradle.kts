plugins {
    application
    kotlin("jvm") version "1.6.10"
}

group = "org.example"
version = "1.0-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("com.example.proto:talk:0.0.4")
    implementation("com.example.proto:delivery:0.0.2")
    runtimeOnly("io.grpc:grpc-netty:1.39.0")
}